package br.com.consiste.crudmysql.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consiste.crudmysql.dto.UsuarioDTO;
import br.com.consiste.crudmysql.model.Dominio;
import br.com.consiste.crudmysql.model.Usuario;

@CrossOrigin
@RequestMapping("/teste")
@RestController
public class Pesquisas {

	@GetMapping("/")
	public ResponseEntity<?> helloWorld(){
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("Clientes-PU");

		EntityManager manager = factory.createEntityManager();

		fazendoProjecoes(manager);

		manager.close();
		factory.close();
		return new ResponseEntity<String>("Hello World!", HttpStatus.OK);
	}
	
	 public static void passandoParametros(EntityManager entityManager) {
	        String jpql = "select u from Usuario u where u.id = :idUsuario";
	        TypedQuery<Usuario> typedQuery = entityManager.
	                createQuery(jpql, Usuario.class).
	                setParameter("idUsuario", 1);
	        Usuario usuario = typedQuery.getSingleResult();
	        System.out.println(usuario.getId() + ", " + usuario.getNome());


	        String jpqlLog = "select u from Usuario u where u.login = :loginUsuario";
	        TypedQuery<Usuario> typedQueryLog = entityManager.
	                createQuery(jpqlLog, Usuario.class).
	                setParameter("loginUsuario", "ria");
	        Usuario usuarioLog = typedQueryLog.getSingleResult();
	        System.out.println(usuarioLog.getId() + ", " + usuarioLog.getNome());
	    }
	
	public static void fazendoProjecoes(EntityManager manager) {
		String jpqlArr = "select id, login, nome from Usuario";
        TypedQuery<Object[]> typedQueryArr = manager.createQuery(jpqlArr, Object[].class);
        List<Object[]> listaArr = typedQueryArr.getResultList();
        listaArr.forEach(arr -> System.out.println(String.format("%s, %s, %s", arr)));


        String jpqlDto = "select new com.algaworks.sistemausuarios.dto.UsuarioDTO(id, login, nome)" +
                "from Usuario";
        TypedQuery<UsuarioDTO> typedQueryDto = manager.createQuery(jpqlDto, UsuarioDTO.class);
        List<UsuarioDTO> listaDto = typedQueryDto.getResultList();
        listaDto.forEach(u -> System.out.println("DTO: " + u.getId() + ", " + u.getNome()));
	}
	
	public static void escolhendoRetorno(EntityManager manager) {
		String sql = "select u.dominio from Usuario u where u.id = 1";
		Query query = manager.createQuery(sql, Dominio.class);
		Dominio dominio = (Dominio) query.getSingleResult();
		System.out.println(dominio.getNome());
	}

	public static void primeirasConsultas(EntityManager manager) {
		//primeira consulta
		String query = "select u from Usuario u";
		TypedQuery<Usuario> tq = manager.createQuery(query, Usuario.class);
		List<Usuario> response = tq.getResultList();
		response.forEach(u -> System.out.println(u.getNome()));

		//segunda consulta
		String query2 = "select u from Usuario u where u.id = 1";
		TypedQuery<Usuario> tq2 = manager.createQuery(query2, Usuario.class);
		Usuario usuario = tq2.getSingleResult();
		System.out.println(usuario.getNome() + " foi o primeiro");

		//terceira consulta
		Query q = manager.createQuery(query2, Usuario.class);
		Usuario usuario2 = (Usuario) q.getSingleResult();
		System.out.println(usuario2.getNome() + " foi o primeiro2");
	}

}
