package br.com.consiste.crudmysql.controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consiste.crudmysql.model.Cliente;

@CrossOrigin
@RequestMapping("/api")
@RestController
public class Controller {
	
	@GetMapping("/")
	public ResponseEntity<?> helloWorld(){
		return new ResponseEntity<String>("Hello World!", HttpStatus.OK);
	}
	
	@GetMapping("/teste")
	public ResponseEntity<?> metodoTeste(){
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("Clientes-PU");
		
		EntityManager manager = factory.createEntityManager();
		
		//procurar
//		Cliente cliente = manager.find(Cliente.class, 1);
		
		//criar
//		Cliente cliente = new Cliente();
//		cliente.setNome("Poké mart");
//		manager.getTransaction().begin();
//		manager.persist(cliente);
//		manager.getTransaction().commit();
		
		//remover
//		Cliente cliente = manager.find(Cliente.class, 4);
//		manager.getTransaction().begin();
//		manager.remove(cliente);
//		manager.getTransaction().commit();
		
		//update junto com find
//		manager.getTransaction().begin();
//		Cliente cliente = manager.find(Cliente.class, 6);
//		cliente.setNome("Gym pokémon");
//		manager.getTransaction().commit();
		
		//update sem find
//		Cliente cliente = new Cliente();
//		cliente.setId(7);
//		cliente.setNome("Casa dos NPC's");
//		manager.getTransaction().begin();
//		manager.merge(cliente);
//		manager.getTransaction().commit();

		
		//pesquisa com tipagem
//		String query = "select c from Cliente c";
//		TypedQuery<Cliente> tq = manager.createQuery(query, Cliente.class);
//		List<Cliente> lista = tq.getResultList();
//		lista.forEach(c -> System.out.println(c.getNome())); 
		
		
		manager.close();
		factory.close();
		return new ResponseEntity<String>("Teste works", HttpStatus.OK);
	}

}
